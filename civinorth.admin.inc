<?php
/**
 * @file civinorth.admin.inc
 * Administration code
 *
 */


/* 
 * Define some constants for making use of the represent.opennorth.ca API
 */

define('CIVINORTH_BOUNDARIES_URL', 'http://represent.opennorth.ca/boundaries/?contains=');
define('CIVINORTH_BOUNDARYSETS_URL', 'http://represent.opennorth.ca/boundary-sets/?limit=1000');
define('CIVINORTH_POSTCODES_URL', 'http://represent.opennorth.ca/postcodes/');
define('CIVINORTH_REPRESENTATIVES_URL', 'http://represent.opennorth.ca/representatives/?point=');
define('CIVINORTH_REPRESENTATIVESETS_URL', 'http://represent.opennorth.ca/representative-sets/?limit=1000');


/**
 * Configure various administrative settings
 */
function civinorth_admin_config($form, &$form_state) {
  $fieldsets = array('' => ' --- ');
  $config = civinorth_get_config(TRUE);
  civicrm_initialize();
  $result = civicrm_api("CustomGroup", "get", array('version' => '3','options' => array('limit' => '0')));
  if (!$result['is_error']) {
    foreach ($result['values'] as $field_group) {
      $fieldsets[$field_group['id']] = $field_group['title'];
    }
  }
  $subtypes = array('' => ' --- ');
  $result = civicrm_api("ContactType", "get", array('version' => '3'));
  if (!$result['is_error']) {
    foreach ($result['values'] as $type) {
      if (!empty($type['parent_id'])) { 
        // an individual subtype
        if ($type['parent_id'] == 1) { 
          $subtypes[$type['name']] = $type['label'];
        }
      }
    }
  }
  $config->boundarysets_url = variable_get('civinorth_boundarysets_url', CIVINORTH_BOUNDARYSETS_URL);
  $config->boundaries_url = variable_get('civinorth_boundaries_url', CIVINORTH_BOUNDARIES_URL);
  $config->postcodes_url = variable_get('civinorth_postcodes_url', CIVINORTH_POSTCODES_URL);
  $config->representativesets_url = variable_get('civinorth_representativesets_url', CIVINORTH_REPRESENTATIVESETS_URL);
  $config->representatives_url = variable_get('civinorth_representatives_url', CIVINORTH_REPRESENTATIVES_URL);
  $form['setstypes'] = array(
    '#type' => 'fieldset',
    '#description' => 'Custom fieldsets and types for use in CiviNorth. Once set you probably don\'t want to change them.',
    '#title' => t('CiviCRM Fields and Types'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($config->fieldset),
    '#weight' => -2,
  );

  $form['setstypes']['civinorth_fieldset'] = array(
    '#type' => 'select',
    '#options' => $fieldsets,
    '#title' => t('CiviNorth Fieldset'),
    '#default_value' => $config->fieldset,
    '#description' => t('The name of the Individual CiviCRM fieldset used to store automated data from CiviNorth. This should be a dedicated fieldset.'),
  );
  $form['setstypes']['civinorth_representative_sub_type'] = array(
    '#type' => 'select',
    '#options' => $subtypes,
    '#title' => t('CiviNorth Representative Subtype'),
    '#default_value' => $config->representative_sub_type,
    '#description' => t('The CiviCRM subtype used for CiviNorthative.'),
  );
  $form['setstypes']['civinorth_representative_fieldset'] = array(
    '#type' => 'select',
    '#options' => $fieldsets,
    '#title' => t('CiviNorth Representative Fieldset'),
    '#default_value' => $config->representative_fieldset,
    '#description' => t('The name of the CiviCRM fieldset used for Representatives in CiviNorth. This should be a dedicated fieldset specific to the subtype above.'),
  );
  /* $form['civinorth_location_type_id'] = array(
   '#type' => 'select',
   '#options' => $location_types,
   '#title' => t('CiviNorth Location Type'),
   '#default_value' => $config->location_type_id,
   '#description' => t('The location type that this module works with, usually "Home"'),
   ); */
  $form['reference_urls'] = array(
    '#type' => 'fieldset',
    '#description' => 'Default URLs for the opennorth service. Mainly for reference, you probably don\'t want to change them.',
    '#title' => t('Opennorth URLs'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['reference_urls']['civinorth_boundarysets_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviNorth BoundarySets REST URL'),
    '#default_value' => $config->boundarysets_url,
    '#description' => t('The URL of the rest interface of the boundary sets'),
  );
  $form['reference_urls']['civinorth_postcodes_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviNorth Postalcode lookup REST URL'),
    '#default_value' => $config->postcodes_url,
    '#description' => t('The URL of the rest interface of the postcode lookup'),
  );
  $form['reference_urls']['civinorth_boundaries_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviNorth Boundaries lookup REST URL'),
    '#default_value' => $config->boundaries_url,
    '#description' => t('The URL of the rest interface of the boundaries'),
  );
  $form['reference_urls']['civinorth_representativesets_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviNorth RepresentativeSets REST URL'),
    '#default_value' => $config->representativesets_url,
    '#description' => t('The URL of the rest interface of the representative sets'),
  );
  $form['reference_urls']['civinorth_representatives_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviNorth Representatives REST URL'),
    '#default_value' => $config->representatives_url,
    '#description' => t('The URL of the rest interface of the representatives'),
  );
  if ($config->fieldset) {
    /* build the allowable options for the fields in the custom fieldset */
    $options = array(
      'civinorth_update' => 'Last Update',
      'civinorth_status' => 'Status Value',
      'civinorth_status_note' => 'Status Note',
    );
    if ($config->boundarysets_url) {
      $options[] = ' --- ';
      $json = file_get_contents($config->boundarysets_url);
      $data = json_decode($json, TRUE);
      foreach ($data['objects'] as $set) {
        $options[$set['url']] = $set['name'];
      }
    }
    if ($config->representativesets_url) {
      $more_options = array();
      $json = file_get_contents($config->representativesets_url);
      $data = json_decode($json, TRUE);
      foreach ($data['objects'] as $set) {
        $more_options[$set['url']] = 'Representatives: ' . $set['name'];
      }
      ksort($more_options);
      $options[] = ' --- ';
      $options = array_merge($options,$more_options);
    }
    $form['civinorth'] = array(
      '#type' => 'fieldset',
      '#description' => 'Fields in the fieldset can be mapped to a boundary set, a representative set, or to one of the administrative functions',
      '#title' => t('CiviNorth Field Settings'),
      '#collapsible' => TRUE,
      '#weight' => -1,
    );
    $result = civicrm_api("CustomField", "get", array('custom_group_id' => $config->fieldset, 'version' => '3','options' => array('limit' => '0')));
    if (!$result['is_error']) {
      // print_r($result['values']); // die();
      // die("test: $civinorth_fieldset");
      foreach ($result['values'] as $id => $field) {
        // $fields[$field['id']] = $field['label'];
        $key = 'civinorth_' . $id;
        $default = variable_get($key, '');
        $form['civinorth'][$key] = array(
          '#type' => 'select',
          '#options' => $options,
          '#title' => $field['label'],
          '#default_value' => $default,
          '#description' => t('Select the administrative function or boundary set that should be used to automatically populate this field'),
        );
      }
    }
  }
  cache_clear_all('civinorth_config', 'cache');
  return system_settings_form($form);
}

/**
 * Enumerate how many contacts have been configured at which status using Civinorth
 *
 * Used on the civinorth administration page to provide an overview of data
 */
function civinorth_status() {
  // $config = civinorth_get_config(TRUE); // reload the config!
  $config = civinorth_get_config();
  if (empty($config->special['civinorth_status'])) {
    return t('Please configure your CiviCRM fields !here.',array('!here' => l('here','admin/config/services/civinorth')));
  }
  civicrm_initialize();
  // return '<pre>'.print_r($config,TRUE).'</pre>';
  /* check how many contacts have been configured with what status */
  // foreach($config->mappings as $id => $map) {
  $status = array(
    0 => 'All',
    1 => 'Set',
    2 => 'Error',
  );
  $status_custom_fid = 'custom_' . $config->special['civinorth_status'];
  $params = array(
    'version' => '3',
    'contact_type' => 'Individual',
  ); // , 'rowCount' => '0');
  $header = array('Status', 'Count');
  $rows = array();
  foreach ($status as $key => $label) {
    if ($key) {
      $params[$status_custom_fid] = $key; // ($key ? "$key" : 'NULL');
    }
    $results = civicrm_api('Contact', 'getcount', $params);
    if (is_array($results) && $results['is_error']) {
      $rows[] = array($label, 'Unexpected error');
    }
    else {
      $rows[] = array($label, $results);
    }
  }
  /*
   unset($params[$status_custom_field]);
   //array('version' => 3, 'contact_id' => $cid, 'return' => array('postal_code','geo_code_1','geo_code_2'), 'location_type_id' => $config->location_type_id));
   $params['postal_code'] = '';
   $result = civicrm_api('Address','getcount',$params);
   $rows[] = array('No postal code',$result);
   unset($params['postal_code']);
   $params['geo_code_1'] = '';
   $params['geo_code_2'] = '';
   $result = civicrm_api('Address','getcount',$params);
   $rows[] = array('No geocode',$result); */
  return theme('table', array('header' => $header, 'rows' => $rows));
} 

/** * Update all CiviCRM Contacts of type 'representative sub type' via opennorth *
 * @param force: overwrite data, not just missing contacts 
 * This function is used to populate the representative contacts in CiviCRM
 * It also updates the boundaryset option groups.
 */
function civinorth_representatives_update($force = FALSE) {
  $config = civinorth_get_config(TRUE);
  // print_r($config); die();
  /* for each of my configured representative sets */
  foreach ($config->mappings as $id => $map) {
    if ($map['type'] == 'boundaryset') {
      // if this boundary set is implemented as a select, cycle through all the boundaries and get my options 
      if (isset($config->option_groups[$id])) {
        // get the current values and params
        $option_group = $config->option_groups[$id]; 
        $old_boundary_options = $option_group['values'];
        $new_boundary_options = array();
        $next = $map['set']['related']['boundaries_url'] . '?limit=1000';
        // calculate prefix path length for later
        $ppl = strlen($map['set']['related']['boundaries_url']);
        while ($next) {
          $url = $config->boundarysets_url_base . $next;
          $json = @file_get_contents($url);
          if (!$json) {
            $next = '';
          }
          else {
            $data = json_decode($json, TRUE);
            $next = $data['meta']['next'];
            foreach ($data['objects'] as $boundary) {
              $key = trim(substr($boundary['url'],$ppl),'/');
              $label = $boundary['name'];
              if (isset($old_boundary_options[$key])) {
                if ($old_boundary_options[$key] != $label) {
                  $new_boundary_options[$key] = $label;
                }
                unset($old_boundary_options[$key]);
              }
              else {
                $new_boundary_options[$key] = $label;
              }
            }
          } 
        }
        foreach($old_boundary_options as $key => $label) {
          $option_params = $option_group['params'];
          $option_params['value'] = $key;
          $result = civicrm_api("OptionValue", "delete", $option_params);
        } 
        foreach($new_boundary_options as $key => $label) {
          $option_params = $config->option_groups[$id]['params'];
          $option_params['value'] = $key;
          $option_params['name'] = $label;
          $option_params['weight'] = '0';
          $result = civicrm_api("OptionValue", "create", $option_params);
        }
      }
    }
    elseif ($map['type'] == 'representativeset') {
      /* we're going to cycle through this rep sets' representatives and match them up with my contacts and create them if necessary */
      // we'll do it batches of 400, possibly too conservative ...
      // $config->test_url = $url;
      /* initialize the parameters used to create new/updated representative contacts */
      $base_params = array(
        'version' => 3,
        'contact_type' => 'Individual',
        'contact_sub_type' => $config->representative_sub_type,
      );
      $ukeys = array();
      $key_fields = array(); // an array of the corresponding civicrm fields that make up the representative's unique key
      foreach ($config->rfield as $key => $rfield) {
        if (isset($rfield['key'])) {
          if (1 == $rfield['key']) {
            $ukeys[] = $key;
            $key_fields[] = $rfield['field'];
          }
        }
      }
      // initialize the next list of up to 400 representatives in this set
      $next = $map['set']['related']['representatives_url'] . '?limit=400';
      while ($next) {
        $url = $config->representativesets_url_base . $next;
        $json = @file_get_contents($url);
        if (!$json) {
          $url = '';
        }
        else {
          $data = json_decode($json, TRUE);
          $next = $data['meta']['next'];
          foreach ($data['objects'] as $rep) {
            $key_values = array();
            foreach ($ukeys as $field) { // I need these fields returned
              $key_values[] = $rep[$field];
            }
            $my_key = implode(' | ', $key_values);
            $contact_id = $config->representative_ids[$my_key]; // from my saved my list of representatives, value = contact id, key = (person name + office + district name) for uniqueness
            if (empty($contact_id) || $force) {
              // save it as a new record
              $my_params = $base_params;
              if ($contact_id) {
                $my_params['id'] = $contact_id;
              }
              foreach ($rep as $key => $value) {
                $field = $config->rfield[$key]['field'];
                $my_params[$field] = _truncate_varchar($rep[$key]);
              }
              $results = civicrm_api('Contact', 'create', $my_params);
              if (0 == $results['is_error']) {
                drupal_set_message(t('Created/updated %my_key', array('%my_key' => $my_key)));
              }
              else {
                drupal_set_message(t('Failed to create/update %my_key', array('%my_key' => $my_key)));
              }
            }
            else {
              drupal_set_message(t('Found matching contact for %my_key', array('%my_key' => $my_key)));
            }
          }
        }
      }
    }
  }
  cache_clear_all('civinorth_config', 'cache'); // cached option_group arrays are no longer valid!
  return 'Finished update';
}

/** 
 * Cron: fill in up to 20 new or unset representation values 
 * 
 * This is just the cron function, called from the hook in the .module file.
 * The represent.opennorth service restricts us to 60 calls per minute, so this cron limit its use to 20.
 * The actual call to the service is in the civinorth_contact_update() function.
 * You'll want to run this cron frequently to populate a big uninitialized contact database.
 * For most small and installs, running 20 updates an hour is probably reasonable.
 * @param $manual if running manually, set this to TRUE to override the usual selection of the status_custom_fid
 */
function _civinorth_cron($opt = array()) {
  $config = civinorth_get_config();
  $status_custom_fid = 'custom_' . $config->special['civinorth_status'];
  $update_custom_fid = 'custom_' . $config->special['civinorth_update'];
  // fetch contact id and address for indivuals that don't have special status set
  $params = array(
    'sequential' => '1',
    'contact_type' => 'Individual',
    'return' => $status_custom_fid,
     $status_custom_fid => array('IS NULL' => 1),
  );
  /* if (0 || !$manual) {
    $params[$status_custom_fid] = 0;
  } */
  try{
    $updates = civicrm_api3('contact', 'get', $params);
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    return;
  }
  if (0 === $updates['count']) {
    $params[$status_custom_fid] = '0';
    $updates = civicrm_api3('contact', 'get', $params);
  }
  // print_r($params);
  // $updates = civicrm_api3('Contact', 'get', $params);
  // print_r($updates); 
  if (0 === $updates['is_error']) {
    foreach ($updates['values'] as $contact) {
      if (!empty($opt['debug'])) {
        drupal_set_message(t('Updating <pre>%contact</pre>', array('%contact' => print_r($contact,TRUE))));
      }
      $cid = $contact['contact_id'];
      $status = 0;
      $address = civicrm_api('Address', 'getsingle', array('version' => 3, 'contact_id' => $cid, 'return' => array('street_address','postal_code', 'geo_code_1', 'geo_code_2','contact_id'), 'location_type_id' => $config->location_type_id));
      if ((!empty($address['geo_code_1']) && !empty($address['geo_code_2']) && !empty($address['street_address'])) || !empty($address['postal_code'])) {
        // error will be either 1 for some kind of unexpected failure, or 0 for success, or higher for a pre-existing error condition
        if (!empty($opt['debug'])) {
          drupal_set_message(t('Updating contact with <pre>%address</pre>', array('%address' => print_r($address,TRUE))));
        }
        $status = 1 + $error = civinorth_contact_update($config, $address); 
        if (2 == $status) {
          $status_note = 'Unexpected error';
        }
      }
      else { // unable to update this contact, mark as "error" = status 2
        $status = 2;
        $status_note = ($address['is_error'] ? $address['error_message'] : 'Missing postal code or street address');
      }
      // If I got an unexpected error, put a note in the corresponding contact record, it probably needs to get an address fix.
      if (2 == $status) {
        $set_params = array(
          'version' => '3',
          'entity_id' => $cid,
        );
        $set_params['custom_' . $config->special['civinorth_update']] = date( "YmdHis" );
        $set_params['custom_' . $config->special['civinorth_status']] = $status; // error
        $set_params['custom_' . $config->special['civinorth_status_note']] = $status_note;
        $result = civicrm_api('CustomValue', 'create', $set_params);
      }
      if (!empty($opt['debug'])) {
        drupal_set_message(t('Updated %name, status %status', array('%name' => $contact['display_name'], '%status' => $status)));
      }
    }
  }
  else {
    if (!empty($opt['debug'])) {
      drupal_set_message(t('Unexpected error <pre>%error</pre>',array('%error' => $updates)));
    }
    else {
      // todo: add to watchdog
    }
  }
  return 'Finished';
}

function _truncate_varchar($string) {
  if (strlen($string) <= 250) {
    return $string;
  }
  return substr($string, 0, 250);
}
