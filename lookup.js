
/**
 * JavaScript behaviors for the front-end display of webforms.
 * @todo: fix the getJSON path to include Drupal's base path 
 */

(function ($) {

Drupal.behaviors.cnLookup = function(context) {
  cnLookupRefresh();
  var pcf = '#edit-submitted-civicrm-1-contact-1-address-postal-code';
  var rpf = '#edit-submitted-civicrm-1-contact-1-cg7-custom-22';
  $(pcf).change(function() {
    var pcode = $(this).val().replace(/ /g,'').toUpperCase(); 
    $.getJSON('/civinorth/lookup-pcode/22/'+pcode,function(data) {
      if (data) {
        /* console.log(data); */
        $(rpf).val(data).trigger('change');
        /* alert('Updated your electoral district for postal code: '+data); */
      }
    });
  });
  $(rpf).change(function(data) { cnLookupRefresh(); });
}

/* function to update mp's email field when district field is changed, either from initial civicrm setting, manual update or via a lookup above */
function cnLookupRefresh() {
  var district = $('#edit-submitted-civicrm-1-contact-1-cg7-custom-22').val();
  /* console.log(district); */
  if (district) {
    $.getJSON('/civinorth/lookup-email/22/'+district,function(result) {
      /* console.log(result); */
      if (result.status == 1) {
        $('#edit-submitted-my-mp').val(result.data.email);
      }
      else {
        alert('Unable to find your MP\'s email for: '+district);
        $('#edit-submitted-my-mp').val('');
      }
    }); 
  }
}

})
